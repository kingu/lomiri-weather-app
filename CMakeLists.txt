project(lomiri-weather-app)
cmake_minimum_required(VERSION 3.0.0)

include(FindGettext)
if(NOT GETTEXT_FOUND)
    message(FATAL_ERROR "Could not find gettext")
endif(NOT GETTEXT_FOUND)

set (MANIFEST_PATH "manifest.json.in" CACHE INTERNAL "Relative path to the manifest file")

find_package(Qt5Core REQUIRED)
find_package(Qt5Qml REQUIRED)
find_package(Qt5Quick REQUIRED)
#find_package(Qt5QuickControls2 REQUIRED)

# Automatically create moc files
set(CMAKE_AUTOMOC ON)

option(INSTALL_TESTS "Install the tests on make install" on)
option(CLICK_MODE "Build as a click package" on)
set(OWM_API_KEY "" CACHE STRING "OpenWeatherMap API key")

# Tests
enable_testing()

# Standard install paths
include(GNUInstallDirs)

set(CLICK_NAME weather.ubports)
set(APP_NAME weather)
set(APP_HARDCODE lomiri-weather-app)
set(MAIN_QML ${APP_HARDCODE}.qml)
set(DESKTOP_FILE "${APP_HARDCODE}.desktop")
set(URLS_FILE "${APP_HARDCODE}.url-dispatcher")
set(ICON weather-app.svg)
set(SPLASH weather-app-splash.svg)
set(AUTOPILOT_DIR lomiri_weather_app)

# Set install paths
if(CLICK_MODE)
  set(CMAKE_INSTALL_PREFIX "/")
  set(LOMIRI_WEATHER_APP_DIR "${CMAKE_INSTALL_DATADIR}/app")

  set(QT_IMPORTS_DIR "${CMAKE_INSTALL_LIBDIR}")
  set(EXEC "qmlscene %u ${LOMIRI_WEATHER_APP_DIR}/${MAIN_QML}")
  set(DATA_DIR /)
  set(URLS_DIR ${DATA_DIR})
  set(ICON ${LOMIRI_WEATHER_APP_DIR}/${ICON})

  set(MODULE_PATH ${QT_IMPORTS_DIR})
else(CLICK_MODE)
  set(DATA_DIR ${CMAKE_INSTALL_DATADIR}/${APP_HARDCODE})
  set(LOMIRI_WEATHER_APP_DIR "${DATA_DIR}/app")
  set(EXEC ${APP_HARDCODE})
  set(ICON ${CMAKE_INSTALL_PREFIX}/${DATA_DIR}/${ICON})
  configure_file(${APP_HARDCODE}.in
    ${CMAKE_CURRENT_BINARY_DIR}/${APP_HARDCODE})
  install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/${APP_HARDCODE}
    DESTINATION ${CMAKE_INSTALL_BINDIR})
  set(DESKTOP_DIR ${CMAKE_INSTALL_DATADIR}/applications)
  set(URLS_DIR ${CMAKE_INSTALL_DATADIR}/lomiri-url-dispatcher/urls)
endif(CLICK_MODE)

if(${CLICK_MODE})
  message("-- Configuring manifest.json")

  configure_file(${MANIFEST_PATH} ${CMAKE_CURRENT_BINARY_DIR}/manifest.json)
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/manifest.json DESTINATION ${CMAKE_INSTALL_PREFIX})
  install(FILES "${APP_HARDCODE}.apparmor" DESTINATION ${CMAKE_INSTALL_PREFIX})
else(CLICK_MODE)
endif()

configure_file(${DESKTOP_FILE}.in.in ${DESKTOP_FILE}.in)

add_custom_target(${DESKTOP_FILE} ALL
    COMMENT "Merging translations into ${DESKTOP_FILE}..."
    COMMAND ${GETTEXT_MSGFMT_EXECUTABLE}
            --desktop --template=${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE}.in
            -o ${DESKTOP_FILE}
            -d ${CMAKE_SOURCE_DIR}/po
)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE}
        DESTINATION ${CMAKE_INSTALL_DATADIR}/applications)

install(FILES ${URLS_FILE} DESTINATION ${URLS_DIR})

add_subdirectory(app)
add_subdirectory(po)
add_subdirectory(tests)

# make the qml files visible on qtcreator
file(GLOB QML_JS_FILES
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    *.qml *.js README* *.txt)

add_custom_target(weather_ubports_QMLFiles DEPENDS lomiri-weather-app.qml SOURCES ${QML_JS_FILES})

# TODO: Add custom target for autopilot and run.
